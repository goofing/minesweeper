package server

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"minesweeper/src/views"
)

type api struct {
	router http.Handler
}

type Server interface {
	Router() http.Handler
}

func New() Server {
	a := &api{}
	r := mux.NewRouter()
	r.HandleFunc("/health", a.health).Methods(http.MethodGet)
	r.HandleFunc("/v1/board/{ID:[a-zA-Z0-9_]+}", views.Get).Methods(http.MethodGet)
	r.HandleFunc("/v1/board", views.Create).Methods(http.MethodPost)
	r.HandleFunc("/v1/board/click", views.MakeCLick).Methods(http.MethodPost)

	a.router = r
	return a
}

func (a *api) Router() http.Handler {
	return a.router
}

func (a *api) health(w http.ResponseWriter, r *http.Request) {
	fmt.Println("HEALTH")
}