package controllers

import (
	"errors"
	"fmt"
	list "github.com/gogoofing/list"
	"math/rand"
	http2 "minesweeper/src/http"
	"minesweeper/src/models"
	"minesweeper/src/repository"
	"minesweeper/src/serializers"
	"net/http"
	"time"
)

var HARD = "HARD"
var MEDIUM = "MEDIUM"
var EASY = "EASY"

type boardController struct {
	board *models.Board
	repository *repository.Board
}

type BoardController interface {
	getRandomPoint() models.Point
	makeRandomMines(minesQty int)
	makeRandomMinesButIn(p *models.Point, minesQty int)
	countMinesAround(p *models.Point) int32
	isChecked(p *models.Point) bool
	setChecked(p *models.Point, value int32)
	getChecked(p *models.Point) *models.PointMap
	isInsideBoard(p *models.Point) bool
	searchBombs(p *models.Point)
	Click(p *models.Point, uuid string) error
	PrintMines()
	IsBomb(point *models.Point) bool
	InitBoard(width int32, height int32, qtyMines int)
	InitBoardOnClicked(width int32, height int32, qtyMines int, p *models.Point, uuid string) error
	InitBoardByDifficulty(difficulty string, uuid string,  p *models.Point) error
}

func (b *boardController) getRandomPoint() models.Point {
	rand.Seed(time.Now().UnixNano())
	x := rand.Int31n(b.repository.Model.Width)
	y := rand.Int31n(b.repository.Model.Height)

	return models.Point{X: x, Y: y}
}

func (b *boardController) makeRandomMines(minesQty int) {
	bomb := b.getRandomPoint()
	for b.repository.Model.Bombs.Length() < minesQty {
		if !b.IsBomb(&bomb) {
			(*b.repository.Model.Bombs).PushBack(bomb)
		}
		bomb = b.getRandomPoint()
	}
}

func (b *boardController) makeRandomMinesButIn(p *models.Point, minesQty int) {
	bomb := b.getRandomPoint()
	for b.repository.Model.Bombs.Length() < minesQty {
		if !b.IsBomb(&bomb) && (bomb.Y != p.Y || bomb.X != p.X ){
			(*b.repository.Model.Bombs).PushBack(bomb)
		}
		bomb = b.getRandomPoint()
	}
}

func (b *boardController) countMinesAround(p *models.Point) int32 {
	var qtyMines int32 = 0

	var point = models.Point{X: p.X, Y: p.Y - 1} // north
	if b.IsBomb(&point){ qtyMines++ }

	point = models.Point{X: p.X, Y: p.Y + 1} // south
	if b.IsBomb(&point){ qtyMines++ }

	point = models.Point{X: p.X - 1, Y: p.Y} // west
	if b.IsBomb(&point){ qtyMines++ }

	point = models.Point{X: p.X + 1, Y: p.Y} // east
	if b.IsBomb(&point){ qtyMines++ }

	point = models.Point{X: p.X - 1, Y: p.Y - 1} // northWest
	if b.IsBomb(&point){ qtyMines++ }

	point = models.Point{X: p.X - 1, Y: p.Y + 1} // southWest
	if b.IsBomb(&point){ qtyMines++ }

	point = models.Point{X: p.X + 1, Y: p.Y + 1} // southEast
	if b.IsBomb(&point){ qtyMines++ }

	point = models.Point{X: p.X + 1, Y: p.Y - 1} // northEast
	if b.IsBomb(&point){ qtyMines++ }

	return qtyMines
}

func (b *boardController) isChecked(p *models.Point) bool {

	if b.repository.Model.Checked.Length() > 0 {
		p1 := b.repository.Model.Checked.GetFirst()

		for b.repository.Model.Checked.Length() > 0 && p1 != nil {
			pm := (p1.Value).(models.PointMap)
			if pm.Point.X == p.X && pm.Point.Y == p.Y{
				return true
			}
			p1 = p1.Next()
		}
	}
	return false
}

func (b *boardController) setChecked(p *models.Point, value int32) {

	b.repository.Model.Checked.PushBack(models.PointMap{Point: *p, Value: value})
}

func (b *boardController) getChecked(p *models.Point) *models.PointMap{
	if b.repository.Model.Checked.Length() > 0 {
		p1 := b.repository.Model.Checked.GetFirst()

		for b.repository.Model.Checked.Length() > 0 && p1 != nil {
			pm := (p1.Value).(models.PointMap)
			if pm.Point.X == p.X && pm.Point.Y == p.Y{
				return &pm
			}
			p1 = p1.Next()
		}
	}
	return nil
}

func (b *boardController) isInsideBoard(p *models.Point) bool{
	return p.Y >= 0 && p.Y <= b.repository.Model.Height && p.X >= 0 && p.X <= b.repository.Model.Height
}

func (b *boardController) searchBombs(p *models.Point) {
	if b.isInsideBoard(p) && !b.isChecked(p){
		var minesAtCurrent = b.countMinesAround(p)
		b.setChecked(p, minesAtCurrent)
		if minesAtCurrent == 0 {
			var point = models.Point{X: p.X, Y: p.Y - 1} // north
			if !b.IsBomb(&point) && b.isInsideBoard(&point) {
				b.searchBombs(&point)
			}
			point = models.Point{X: p.X, Y: p.Y + 1} // south
			if !b.IsBomb(&point) && b.isInsideBoard(&point) {
				b.searchBombs(&point)
			}
			point = models.Point{X: p.X - 1, Y: p.Y} // west
			if !b.IsBomb(&point) && b.isInsideBoard(&point) {
				b.searchBombs(&point)
			}
			point = models.Point{X: p.X + 1, Y: p.Y} // east
			if !b.IsBomb(&point) && b.isInsideBoard(&point) {
				b.searchBombs(&point)
			}
			point = models.Point{X: p.X - 1, Y: p.Y - 1} // northWest
			if !b.IsBomb(&point) && b.isInsideBoard(&point) {
				b.searchBombs(&point)
			}
			point = models.Point{X: p.X - 1, Y: p.Y + 1} // southWest
			if !b.IsBomb(&point) && b.isInsideBoard(&point) {
				b.searchBombs(&point)
			}
			point = models.Point{X: p.X + 1, Y: p.Y + 1} // southEast
			if !b.IsBomb(&point) && b.isInsideBoard(&point) {
				b.searchBombs(&point)
			}
			point = models.Point{X: p.X + 1, Y: p.Y - 1} // northEast
			if !b.IsBomb(&point) && b.isInsideBoard(&point) {
				b.searchBombs(&point)
			}
		}
	}
}

func (b *boardController) Click(p *models.Point, uuid string) error {
	if !b.isInsideBoard(p){
		err := errors.New("POINT OUT OF BOARD")
		return err
	}
	if b.IsBomb(p) {
		b.repository.Model.ClickedOnMine = true
		return nil
	}
	b.searchBombs(p)
	b.repository.Find(uuid)
	return nil
}

func (b *boardController) PrintChecked() {
	var i int32 = 0
	var j int32
	fmt.Printf("\n")
	for i < b.repository.Model.Width {
		j = 0
		for j < b.repository.Model.Height {
			p := models.Point{X: i, Y: j}
			val := b.getChecked(&p)
			if val != nil{
				fmt.Printf("%v	", val.Value * 10)
			}else {
				fmt.Printf("-1	")
			}
			j++
		}
		fmt.Printf("\n")
		i++
	}
}

func (b *boardController) PrintMines() {
	var i int32 = 0
	var j int32
	fmt.Printf("\n")
	for i < b.repository.Model.Width {
		j = 0
		for j < b.repository.Model.Height {
			var p = models.Point{X: i, Y: j}
			var isMineAdded = b.IsBomb(&p)

			if isMineAdded {
				fmt.Printf("-2	")
			} else {
				fmt.Printf("0	")
			}
			j++
		}
		fmt.Printf("\n")
		i++
	}
}

func (b *boardController) IsBomb(point *models.Point) bool {
	p1 := (*b.repository.Model.Bombs).GetFirst()
	for p1 != nil {
		p2 := (p1.Value).(models.Point)
		if p2.X == point.X && p2.Y == point.Y {
			return true
		}
		p1 = p1.Next()
	}
	return false
}

func (b *boardController) InitBoard(width int32, height int32, qtyMines int) {
	var c = list.New()
	var m = list.New()
	var board = &models.Board{Checked: c, Bombs: m, Width: width, Height: height}
	b.repository.Model = board
	b.makeRandomMines(qtyMines)
}

func (b *boardController) InitBoardOnClicked(width int32, height int32, qtyMines int, p *models.Point, uuid string) error{
	var c = list.New()
	var m = list.New()
	var board = &models.Board{Checked: c, Bombs: m, Width: width, Height: height}
	b.repository.Model = board

	b.makeRandomMinesButIn(p, qtyMines)
	err := b.Click(p, uuid)
	return err
}

func (b *boardController) InitBoardByDifficulty(difficulty string, uuid string,  p *models.Point) (*http2.Message, *http2.Message){
	var c = list.New()
	var m = list.New()
	switch difficulty {
	case HARD:
		b.repository.Model = &models.Board{Checked: c, Bombs: m, Width: 22, Height: 18}
		b.makeRandomMinesButIn(p, 100)

	case MEDIUM:
		b.repository.Model = &models.Board{Checked: c, Bombs: m, Width: 14, Height: 12}
		b.makeRandomMinesButIn(p, 50)

	case EASY:
		b.repository.Model = &models.Board{Checked: c, Bombs: m, Width: 10, Height: 9}
		b.makeRandomMinesButIn(p, 10)
	}
	err := b.Click(p, uuid)
	if err != nil {
		return nil, &http2.Message{StatusCode: http.StatusBadRequest, Status: http2.StatusError, Content: http2.RequestSuccessfully}
	}
	err = b.repository.Create(uuid)
	if err != nil {
		return nil, &http2.Message{StatusCode: http.StatusBadRequest, Status: http2.StatusError, Content: http2.RequestSuccessfully}
	}
	return &http2.Message{StatusCode: http.StatusCreated, Status: http2.StatusOk,
		Content: serializers.Serialize(b.repository.Model)}, nil
}

func (b *boardController) GetBoard() *models.Board {
	return  b.repository.Model
}

func NewBoardController() *boardController {
	repo := repository.Init()
	return &boardController{repository: repo}
}
