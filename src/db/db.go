package db

import "github.com/go-redis/redis"

var Instance *redis.Client
var err error

func GetInstance () *redis.Client{
	if Instance == nil{
		Instance = redis.NewClient(&redis.Options{
			Addr: ":6379",
		})
	}

	if err != nil {
		panic("failed to connect database")
	}

	return Instance
}
