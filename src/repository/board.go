package repository

import (
	"fmt"
	"minesweeper/src/db"
	"minesweeper/src/models"
	"minesweeper/src/serializers"
	"time"
)

func (r *Board) Find(key string) *models.Board {
	var board = &models.Board{}
	var val = r.db.Get(key)
	fmt.Printf("%v", val)
	return board
}

func (r *Board) Create(key string) error {
	var d, err = time.ParseDuration("30s")

	serialized := serializers.Serialize(r.Model)
	if err == nil {
		errRed := r.db.Set(key, serialized, d)
		if _, e := errRed.Result(); e != nil {
			fmt.Printf("\n %v", e)
		}
	}
	return nil
}

func Init() *Board {

	return &Board{Model: &models.Board{}, db: db.GetInstance()}
}
