package repository

import (
	"github.com/go-redis/redis"
	"minesweeper/src/models"
)

type Board struct {
	Model *models.Board
	db *redis.Client
}

type IUser interface {
	FindById(id int) (*Board, error)
	Create(board *Board) error
	Init()
}


