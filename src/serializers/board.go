package serializers

import (
	"encoding/json"
	"fmt"
	"io"
	"minesweeper/src/models"
)



func Serialize(board *models.Board) *models.SerializedBoard{
	serializedBoard := models.SerializedBoard{ClickedOnMine: board.ClickedOnMine, Checked: []models.PointMap{}, Mines: []models.Point{}}
	i := int32(0)
	p := board.Bombs.GetFirst()
	for int(i) < board.Bombs.Length() {
		serializedBoard.Mines = append(serializedBoard.Mines, (p.Value).(models.Point))
		p = p.Next()
		i++
	}

	if board.Checked.Length() > 0 {
		i = 0
		p1 := board.Checked.GetFirst()

		for int(i) < board.Checked.Length() {
			pointMap := (p1.Value).(models.PointMap)
			serializedBoard.Checked = append(serializedBoard.Checked, pointMap)
			i++
		}
	}

	return &serializedBoard
}

func Deserialize(body io.ReadCloser) *models.PointDifficulty {
	var point models.PointDifficulty
	decoder := json.NewDecoder(body)
	err := decoder.Decode(&point)
	if err != nil{
		fmt.Printf("got error : %v \n", err)
	}
	return &point
}
