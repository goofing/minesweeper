package views

import (
	//"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	http2 "minesweeper/src/http"
	"net/http"
	//"strconv"
	"minesweeper/src/controllers"
	"minesweeper/src/serializers"
)

var boardController = controllers.NewBoardController()

func Get(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	//id, errId := strconv.ParseInt(vars["ID"],10,64)
	//if errId != nil {
	//	fmt.Printf("error : %v \n", errId)
	//	w.WriteHeader(500)
	//	return
	//}
	fmt.Printf("getting user by id: %v \n", vars["ID"])


	//w.Header().Set("Content-Type", "application/json; charset=utf-8")
	//encoder := json.NewEncoder(w)
	//errJson := encoder.Encode(user)
	//if errJson == nil {
	//	fmt.Printf("error : %v \n", errJson)
	//	w.WriteHeader(500)
	//}
	//TODO: RESPONDER ERRORES 4XX CON BODY, NO SE POR QUE NO SE RESPONDE
}

func Create(w http.ResponseWriter, r *http.Request) {
	point := serializers.Deserialize(r.Body)
	uuid := r.Header.Get("x-global-id")
	res, err := boardController.InitBoardByDifficulty(point.Difficulty, uuid, &point.Point)

	if err != nil {
		fmt.Printf("error creating board : %v \n", err)
		http2.SetErrorResponse(err, w)
		return
	}

	http2.SetOkResponse(res, w)
}


func MakeCLick(w http.ResponseWriter, r *http.Request) {
	pointDiff := serializers.Deserialize(r.Body)
	var err = boardController.Click(&pointDiff.Point, pointDiff.Difficulty)
	if err == nil{
		fmt.Printf("got user : %v \n", err)
	}
	//errJson := serializers.Serialize(w, board)
	//if errJson == nil{
	//	fmt.Printf("error : %v \n", errJson)
	//	w.WriteHeader(500)
	//}
	//w.WriteHeader(200)
}

