package models

type Point struct {
	X int32  `json:"x"`
	Y int32  `json:"y"`
}


type PointMap struct {
	Point	Point
	Value 	int32	`json:"value"`
}

type PointDifficulty struct {
	Point	Point
	Difficulty 	string	`json:"difficulty"`
}
