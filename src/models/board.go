package models

import (
	"encoding/json"
	list "github.com/gogoofing/list"
)

type Board struct {
	ClickedOnMine bool
	Checked       *list.List
	Bombs         *list.List
	Width         int32
	Height        int32
}

type SerializedBoard struct {
	ClickedOnMine bool			`json:"clicked_on_mine"`
	Checked       []PointMap	`json:"points_checked"`
	Mines         []Point		`json:"mines"`
}

func (s *SerializedBoard) MarshalBinary() ([]byte, error) {
	return json.Marshal(s)
}