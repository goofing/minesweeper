module minesweeper

go 1.15

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gogoofing/list v1.0.2
	github.com/gorilla/mux v1.8.0
)
