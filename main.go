package main

import (
	"fmt"
	"log"
	"minesweeper/src/db"
	"minesweeper/src/server"
	"net/http"
)

func main() {
	var name string
	name = "Starting server at 8080"
	fmt.Println(name)
	dbInstance := db.GetInstance()
	fmt.Printf("Connected to redis %v", dbInstance)
	s := server.New()
	log.Fatal(http.ListenAndServe(":8080", s.Router()))


	//var point = models.Point{X: 0, Y: 0}
	//var contr = controllers.NewBoardController()
	//contr.InitBoardOnClicked(5, 4, 5, &point, "")
	//
	//contr.PrintMines()
	//contr.PrintChecked()

}
